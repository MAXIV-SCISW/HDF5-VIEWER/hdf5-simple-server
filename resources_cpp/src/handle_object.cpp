/*
 * This is a C++ version of the orignal python code that can be found in:
 *     resources_py/handle_object.py
 *
 * The reason for making a C++ version is that the python version is not fast
 * enough, especially when multiple sequential calls are made by a web
 * application - it makes the application feel too slow.
 *
 * This code makes use of the c++ wrapper:
 *     h5cpp wrapper - https://github.com/ess-dmsc/h5cpp
 * Using this wrapper is much, much easier and straightforward that using the
 * HDF5 C-API, and the speeds of execution seems to be the same from what I can
 * tell.
 */

// SOURCE FILES
#include "handle_object.hpp"

// The primary function - given a folder, file, or complete dataset path,
// return a json object containing the results
void getObject(const string& inputString, int maxImageSize,
    string configFilePath, bool jsonOutput, bool debug)
{

    if (debug) {
        cout << "*** START getObject ***" << endl;
    }

    // Output variables
    map <int, map<string, string > > file_contents_object;
    map <string, string> general_file_info;
    vector < vector <float> > image;
    vector <int> shape;
    vector <int> section;
    vector <float> bpr_limits;

    // Get some basic information about this object: type, readble or not,
    // exists or not
    getBasicObjectInfoOutput basicOutput = getBasicObjectInfo(
        inputString, configFilePath, debug);
    general_file_info = basicOutput.general_object_info;

    // If this object exists and is accessible to the user, see what it is
    if (basicOutput.does_file_exist && basicOutput.can_user_read_file) {

        // If this is simply a folder on disk
        if (basicOutput.object_type == "folder") {

            // Get the folder contents
            getFolderOutput folderResult = getFolderContents(
                basicOutput.file_path, configFilePath, debug);

            // Add to the output map
            file_contents_object = folderResult.file_contents_object;
        }

        // If this is an hdf5 file, or an object with in a file
        if (basicOutput.object_type == "h5_object") {

            if (basicOutput.item_type != "h5_does_not_exist") {

                try {

                    // Open the file
                    file::File DataFile = openFile(basicOutput.file_path, debug);

                    // Get the root group of the file
                    auto root_group = DataFile.root();

                    // If this is a dataset
                    if (basicOutput.item_type == "line" ||
                        basicOutput.item_type == "image" ||
                        basicOutput.item_type == "image-series" ||
                        basicOutput.item_type == "text" ||
                        basicOutput.item_type == "number") {

                        // Get the dataset
                        getDatasetOutput datasetResult = getDataset(root_group,
                            basicOutput.h5_path, basicOutput.slice_par,
                            maxImageSize, debug);

                        // Add to the output map
                        for(auto const &ent1 : datasetResult.general_file_info) {
                            general_file_info[ent1.first] = ent1.second;
                        }

                        // Save some things for pretty json printing
                        image = datasetResult.image;
                        shape = datasetResult.shape;
                        section = datasetResult.section;
                        bpr_limits = datasetResult.bpr_limits;

                    }

                    // If this is a file or folder within a file
                    if (basicOutput.item_type == "h5_file" ||
                        basicOutput.item_type == "h5_folder") {

                        // Get the contents of the file or folder
                        getFolderOutput folderResult = getHdf5FolderContents(
                            root_group, basicOutput.file_path,
                            basicOutput.h5_path, configFilePath, debug);

                        // Add to the output maps
                        file_contents_object = folderResult.file_contents_object;
                        for(auto const &ent1 : folderResult.general_file_info) {
                            general_file_info[ent1.first] = ent1.second;
                        }
                    }

                } catch (std::runtime_error) {
                    // Getting to this point probably means something is wrong
                    // with the file, it's not really an hdf5 file?
                    cout << "** Something wrong with this file?? **";
                    cout << "*  file_path: " << basicOutput.file_path << endl;
                    cout << "*  h5_path:   " << basicOutput.h5_path << endl;
                    cout << "**************************************" << endl;
                    cout << endl;
                }
            }
        }

    }

    // Print all necessary data to send to the REST application
    if (jsonOutput) {
        printJsonReadyOutput(image, shape, section, bpr_limits,
            general_file_info, file_contents_object, debug);
    } else {
        printTerminalOutput(image, shape, section, bpr_limits,
            general_file_info, file_contents_object, debug);
    }

    if (debug) {
        cout << "***  END  getObject ***" << endl;
    }

    return;
}


getBasicObjectInfoOutput getBasicObjectInfo(const string& inputString,
    string configFilePath, bool debug)
{

    // Output variables
    map <string, string> general_object_info;
    string object_type = "";
    string item_type = "";
    string file_path = "";
    string h5_path = "";
    string error = "";
    vector <int> slice_par;
    bool does_file_exist = false;
    bool can_user_read_file = false;

    // Parse the input string and try to guess whether this is a folder, file,
    // folder in a file, or an object in a file
    ParsedPathInfo parseResult = parseObjectPath(inputString, configFilePath,
        debug);
    object_type = parseResult.object_type;
    slice_par = parseResult.slice_par;

    if (debug) {
        cout << endl;
        cout << "  object_type:         " << object_type << endl;
        cout << "  file_path:           " << parseResult.file_path << endl;
        cout << "  h5_path:             " << parseResult.h5_path << endl;
        cout << "  slices? -            ";
        if (parseResult.slice_par.size() > 0) {
            cout << "yes:" << endl;
        } else {
            cout << "no" << endl;
        }
        for (unsigned i=0; i < parseResult.slice_par.size(); i++) {
            cout << "      slice_par[" << i << "]: ";
            cout << parseResult.slice_par[i] << endl;
        }
        cout << endl;
    }

    // Figure out more precisely what this is, check what the path is really
    // pointing to - a folder, file, image, etc.
    getItemTypeOutput itemTypeResult = getItemType(
        parseResult.file_path + "/" + parseResult.h5_path, configFilePath,
        debug);
    item_type = itemTypeResult.item_type;
    file_path = itemTypeResult.file_path;
    h5_path = itemTypeResult.h5_path;
    error = itemTypeResult.error;

    // Check if the filepath exists and if the user has the necessary
    // permissions to view it
    does_file_exist = doesFileExist(parseResult.file_path);
    can_user_read_file = canUserReadFile(parseResult.file_path);

    if (debug) {
        cout << "  item_type:           " << item_type << endl;
        cout << "  file_path:           " << file_path << endl;
        cout << "  h5_path:             " << h5_path << endl;
        cout << "  error:               " << error << endl;
        cout << "  does_file_exist:     " << does_file_exist << endl;
        cout << "  can_user_read_file:  " << can_user_read_file << endl;
    }

    // Set some of the output - general information, simple string values
    general_object_info["object_type"] = object_type;
    general_object_info["item_type"] = item_type;
    general_object_info["user_can_read"] = (can_user_read_file ? "True": "False");
    general_object_info["does_exist"] = (does_file_exist ? "True": "False");
    general_object_info["path"] = cleanFilePathString(
        parseResult.file_path + "/" + parseResult.h5_path);
    general_object_info["short_name"] = shortNameFromFilePath(
        general_object_info["path"]);
    if (error != "") {
        general_object_info["error"] = error;
    }

    return {general_object_info, object_type, item_type, file_path, h5_path,
        slice_par, does_file_exist, can_user_read_file};
}


// The secondary function - given a folder, file, or complete dataset path,
// return a json object containing the contents of every folder that is an
// ancestor to this object
void getObjectTreePath(const string& inputString, string configFilePath,
    bool jsonOutput, bool debug)
{

    /* TODO
     *      - clean up code
     */

    if (debug) {
        cout << "*** START getObjectTreePath ***" << endl;
    }

    // Output variables
    map <string, map <int, map <string, string> > > tree_path_contents;
    map <string, map <string, string> > general_contents;
    map <string, string> general_object_info;

    // Get some basic information about this object: type, readble or not,
    // exists or not
    getBasicObjectInfoOutput basicOutput = getBasicObjectInfo(
        inputString, configFilePath, debug);
    general_object_info = basicOutput.general_object_info;

    // Parse input - directory? file? directory in a file? object in a file?
    vector <string> tree_path = parseObjectTreePath(inputString,
        configFilePath, debug);

    if (debug) {
        cout << endl;
        cout << "  tree_path:           " << endl;
        for (unsigned i=0; i < tree_path.size(); i++) {
            cout << "      tree_path[" << i << "]: ";
            cout << tree_path[i] << endl;
        }
        cout << endl;
    }

    // Loop over each entry in the tree path array
    for (unsigned i=0; i < tree_path.size(); i++) {

        if (debug) {
            cout << "      Examining tree_path[" << i << "]: ";
            cout << tree_path[i] << endl;
        }

        // Get the type of item, should be one of:
        //    folder, h5_file, h5_folder
        getItemTypeOutput itemTypeResult = getItemType(
            tree_path[i], configFilePath, false);
        string item_type = itemTypeResult.item_type;
        string file_path = itemTypeResult.file_path;
        string h5_path = itemTypeResult.h5_path;
        bool can_user_read_file = canUserReadFile(file_path);
        bool does_file_exist = doesFileExist(file_path);

        if (debug) {
            cout << "        item_type:          " << item_type << endl;
            cout << "        file_path:          " << file_path << endl;
            cout << "        h5_path:            " << h5_path << endl;
            cout << "        can_user_read_file: " << can_user_read_file;
            cout << endl;
            cout << "        does_file_exist:    " << does_file_exist << endl;
        }

        if (can_user_read_file && does_file_exist) {
            general_contents[to_string(i)]["object_type"] = item_type;
            general_contents[to_string(i)]["user_can_read"] = (
                can_user_read_file ? "True": "False");
            general_contents[to_string(i)]["path"] = cleanFilePathString(
                file_path + "/" + h5_path);
            general_contents[to_string(i)]["short_name"] =
                shortNameFromFilePath(general_contents[to_string(i)]["path"]);

            // If this is a folder on disk, get the contents
            if (item_type == "folder") {

                getFolderOutput folderResult = getFolderContents(
                    tree_path[i], configFilePath, debug);

                // Add to the output map
                tree_path_contents[to_string(i)] =
                    folderResult.file_contents_object;
            }

            if (item_type == "h5_file" || item_type == "h5_folder") {

                // Open the file, get the root group of the file
                file::File DataFile = openFile(file_path, debug);
                auto root_group = DataFile.root();

                // Get the file contents
                getFolderOutput folderResult = getHdf5FolderContents(
                    root_group, file_path, h5_path, configFilePath, debug);

                // Add to the output map
                tree_path_contents[to_string(i)] =
                    folderResult.file_contents_object;

                if (item_type == "h5_folder") {

                    // Add to the output map
                    general_contents[to_string(i)]["object_type"] =
                        "h5_folder";
                }

            }
        }

    }

    printTreePathOutput(general_object_info, general_contents,
        tree_path_contents, debug);

    return;
}


// Print key-value pairs in a simple map
void printSimpleMapContents(map <string, string> simple_map, string extra_space)
{
    for (auto const & pair : simple_map) {

        auto const & key   = string(pair.first);
        auto const & value = string(pair.second);

        cout << extra_space << "  '" << key << "': ";
        cout << "'" << value << "', " << endl;
    }

}


void printTreePathOutput(
    map <string, string> general_object_info,
    map <string, map <string, string> > general_contents,
    map <string, map <int, map <string, string> > > tree_path_contents,
    bool debug)
{
    if (debug) {
        cout << endl;
        cout << "*** START printTreePathOutput *** " << endl;
        cout << endl;
    }

    // The REST application expects to see the json output in between two
    // keywords
    cout << "OUTPUT_DICTIONARY" << endl;
    cout << "{" << endl;

    cout << "  '" << "object_info" << "': {" << endl;
    printSimpleMapContents(general_object_info, "  ");
    cout << "  }," << endl;

    cout << "  '" << "tree_path" << "': {" << endl;
    for(auto const &outer_map_pair : tree_path_contents) {
        int count = 0;

        auto const &outer_index_key = outer_map_pair.first;
        cout << "    '" << outer_index_key << "': {" << endl;

        // general_contents
        if (general_contents.find(outer_index_key) !=
            general_contents.end()) {
            printSimpleMapContents(general_contents[outer_index_key], "    ");
        } else {
            if (debug) {
                cout << "** outer_index_key [" << outer_index_key << "] ";
                cout << "not found in general_contents" << endl;
            }
        }

        for(auto const &ent1 : outer_map_pair.second) {

            if (count == 0) {
                cout << "      '" << "folder_contents" << "': {" << endl;
            }
            auto const &index_key = ent1.first;
            cout << "        '" << index_key << "': {" << endl;

            printSimpleMapContents(ent1.second, "        ");

            cout << "        }," << endl;

            count++;
        }

        if (count > 0) {
            cout << "      }," << endl;
        }

        cout << "    }," << endl;
    }
    cout << "  }" << endl;

    cout << "}" << endl;
    cout << "OUTPUT_DICTIONARY" << endl;
}


getDatasetOutput getDataset(hdf5::node::Group root_group, string h5_path,
    vector <int> slice_par, int maxImageSize, bool debug)
{

    if (debug) {
        cout << endl;
        cout << "*** START handle_object.cpp getDataset() ***" << endl;
        cout << endl;
    }

    // Get the dataset
    auto Dataset = root_group.get_dataset(h5_path);

    // Try to figure out what type of dataset this is - there must
    // be a better way to do this?!?
    auto Dataspace = Dataset.dataspace();
    auto DataspaceType = Dataspace.type();

    string dataset_type = "";
    map<string, string> general_file_info;
    vector< vector <float> > image;
    vector <int> shape;
    vector <int> section;
    vector <float> bpr_limits;

    if (DataspaceType == hdf5::dataspace::Type(H5S_SCALAR)) {
        dataset_type = "scalar";
    }

    if (DataspaceType == hdf5::dataspace::Type(H5S_SIMPLE)) {
        dataset_type = "simple";
    }

    if (debug) {
        cout << "  DataspaceType:  " << DataspaceType << endl;
        cout << "  dataset_type:   " << dataset_type << endl;
    }

    // H5S_SCALAR - strings and numbers are of this type
    if (dataset_type == "scalar") {

        getScalarOutput datasetResult = getScalar(Dataset, debug);

        general_file_info["item_type"] = datasetResult.object_type;
        general_file_info["values"] = datasetResult.string_value;
        general_file_info["error"] = datasetResult.error;
    }

    // H5S_SIMPLE - Images, images-series, arrays are all of this type
    if (dataset_type == "simple") {

        // Get the image and information about it. Slice if asked for,
        // and decimate if needed.
        getImageOutput datasetResult = getImage(Dataset, slice_par,
            maxImageSize, debug);

        // Save the information to a dictionary that will later be
        // output in json format to send to a web application
        shape = datasetResult.shape;
        image = datasetResult.image;
        section = datasetResult.section;
        bpr_limits = datasetResult.bpr_limits;

        general_file_info["item_type"] = datasetResult.object_type;

        general_file_info["shape"] = "";
        general_file_info["values"] = "";
        general_file_info["bpr_limits"] = "";
        general_file_info["section"] = "";
        general_file_info["bad_pixels_exist"] = (
            datasetResult.bad_pixels_exist ? "True": "False");
        general_file_info["is_downsampled"] = (
            datasetResult.is_downsampled ? "True": "False");
        general_file_info["error"] = datasetResult.error;

        if (debug) {
            cout << "image.size():    " << image.size() << endl;
            if (image.size() > 0) {
                cout << "image[0].size(): " << image[0].size()
                    << endl;
            }
        }
    }

    return {general_file_info, image, shape, bpr_limits, section};
}


void printJsonReadyOutput(vector< vector <float> > image,
    vector <int> shape, vector <int> section, vector <float> bpr_limits,
    map<string, string> general_file_info,
    map<int, map<string, string > > file_contents_object, bool debug)
{

    if (debug) {
        cout << endl;
        cout << "*** START printJsonReadyOutput *** " << endl;
        cout << endl;
    }

    // The REST application expects to see the json output in between two
    // keywords
    cout << "OUTPUT_DICTIONARY" << endl;
    cout << "{" << endl;

    // Print the contents of general file info, plus dataset values
    for(auto const &ent1 : general_file_info) {

        auto const &inner_key   = string(ent1.first);
        auto const &inner_value = string(ent1.second);

        cout << "  '" << inner_key << "': ";

        if (inner_key.compare("shape") == 0) {

            cout << "(";
            for (int j=0; j < shape.size(); j++) {
                cout << shape[j];
                if (j < shape.size() - 1)
                    cout << ", ";
            }
            cout << ")";

        } else if (inner_key.compare("section") == 0) {

            cout << "(";
            for (int j=0; j < section.size(); j++) {
                cout << section[j];
                if (j < section.size() - 1)
                    cout << ", ";
            }
            cout << ")";

        } else if (inner_key.compare("bpr_limits") == 0) {

            cout << "(";
            for (int j=0; j < bpr_limits.size(); j++) {
                cout << bpr_limits[j];
                if (j < bpr_limits.size() - 1)
                    cout << ", ";
            }
            cout << ")";

        } else if (inner_key.compare("values") == 0) {

            if (shape.size() == 0) {
                cout << "'" << inner_value << "'";
            } else {
                if (shape.size() > 1) {
                    cout << "[";
                }
                for (int j=0; j < image.size(); j++) {

                    cout << "[";
                    for (int i = 0; i < image[j].size(); i++) {

                        cout << image[j][i];
                        if (i < image[j].size() - 1)
                            cout << ", ";
                    }
                    if (j < image.size() - 1)
                        cout << "], ";
                    else
                        cout << "]";
                }
                if (shape.size() > 1) {
                    cout << "]";
                }
            }
        }
        else {
            cout << "'" << inner_value << "'";
        }
        cout << ", " << endl;
    }

    // Print the contents of a file or folder, if any
    int count = 0;
    for(auto const &ent1 : file_contents_object) {

        if (count == 0)
            cout << "  'folder_contents': {" << endl;

        auto const &index_key = ent1.first;
        cout << "    '" << index_key << "': {" << endl;

        for(auto const &ent3 : ent1.second) {

            auto const &inner_key   = string(ent3.first);
            auto const &inner_value = string(ent3.second);

            cout << "      '" << inner_key << "': ";
            cout << "'" << inner_value << "', " << endl;
        }
        cout << "    }," << endl;

        count++;
    }
    if (count > 0) {
        cout << "  }" << endl;
    }

    cout << "}" << endl;
    cout << "OUTPUT_DICTIONARY" << endl;

    if (debug) {
        cout << endl;
        cout << "*** END printJsonReadyOutput *** " << endl;
        if (shape.size() > 1) {
            cout << "image.size():    " << image.size() << endl;
            if (image.size() > 0) {
                cout << "image[0].size(): " << image[0].size() << endl;
            }
        }
        cout << endl;
    }
}


void printTerminalOutput(vector< vector <float> > image,
    vector <int> shape, vector <int> section, vector <float> bpr_limits,
    map<string, string> general_file_info,
    map<int, map<string, string > > file_contents_object, bool debug)
{

    if (debug) {
        cout << endl;
        cout << "*** START printTerminalOutput *** " << endl;
        cout << endl;
    }

    cout << endl;

    // Print the full path name for the object
    string path = general_file_info["path"];
    cout << "  " << path << endl;
    string item_type = general_file_info["item_type"];
    cout << "    --> " << item_type << endl;

    // Check if there was an error.
    bool error_found = false;
    string error;
    if (general_file_info.count("error") == 1){
        error = general_file_info["error"];
        if (error.compare("False") != 0) {
            error_found = true;
        }
    }

    // Print the error
    if (error_found) {
        cout << "      --> ERROR: " << error << endl;
    }

    // Print the contents of general file info, plus dataset values
    for(auto const &ent1 : general_file_info) {

        auto const &inner_key   = string(ent1.first);
        auto const &inner_value = string(ent1.second);

        if (inner_key.compare("shape") == 0) {

            cout << "(";
            for (int j=0; j < shape.size(); j++) {
                cout << shape[j];
                if (j < shape.size() - 1)
                    cout << ", ";
            }
            cout << ")";

        } else if (inner_key.compare("section") == 0) {

            cout << "(";
            for (int j=0; j < section.size(); j++) {
                cout << section[j];
                if (j < section.size() - 1)
                    cout << ", ";
            }
            cout << ")";

        } else if (inner_key.compare("bpr_limits") == 0) {

            cout << "(";
            for (int j=0; j < bpr_limits.size(); j++) {
                cout << bpr_limits[j];
                if (j < bpr_limits.size() - 1)
                    cout << ", ";
            }
            cout << ")";

        } else if (inner_key.compare("values") == 0) {

            if (!error_found) {
                if (shape.size() == 0) {
                    cout << "        --> " << inner_value << endl;
                } else {
                    if (shape.size() > 1) {
                        cout << "[";
                    }
                    for (int j=0; j < image.size(); j++) {

                        cout << "[";
                        for (int i = 0; i < image[j].size(); i++) {

                            cout << image[j][i];
                            if (i < image[j].size() - 1)
                                cout << ", ";
                        }
                        if (j < image.size() - 1)
                            cout << "], ";
                        else
                            cout << "]";
                    }
                    if (shape.size() > 1) {
                        cout << "]";
                    }
                }
            }
        }

    }


    // Print the contents of a file or folder, if any
    int count = 0;
    for(auto const &ent1 : file_contents_object) {

        if (count == 0) {
            cout << endl;
            cout << "  contents:" << endl;
        }

        auto const &index_key = ent1.first;

        string full_name = file_contents_object[index_key]["full_name"];
        string item_type = file_contents_object[index_key]["item_type"];
        cout << "    " << full_name << endl;
        if (file_contents_object[index_key].count("error") == 1){
            string error = file_contents_object[index_key]["error"];
            if (error.compare("False") != 0) {
                cout << "      --> " << item_type << endl;
            } else {
                cout << "      --> ERROR: " << error << endl;
            }
        }

        count++;
    }

    if (debug) {
        cout << endl;
        cout << "*** END printTerminalOutput *** " << endl;
        if (shape.size() > 1) {
            cout << "image.size():    " << image.size() << endl;
            if (image.size() > 0) {
                cout << "image[0].size(): " << image[0].size() << endl;
            }
        }
        cout << endl;
    }
}


// Print a help and usage message for terminal use
void printHelp()
{
    cout << endl;
    cout << "Usage:" << endl;
    cout << "   ./handle_object /path/to/folder/" << endl;
    cout << "   ./handle_object /path/to/folder/hdf5_file.h5" << endl;
    cout << "   ./handle_object /path/to/folder/hdf5_file.h5/";
    cout << "folder_in_file/dataset" << endl;
    cout << endl;
    cout << "Options:" << endl;
    cout << "   -h                    show this help message and exit" << endl;
    cout << "   -d                    debug output" << endl;
    cout << "   -j                    json formatted output, default is" << endl;
    cout << "                         output in terminal friendly format" << endl;
    cout << "   -m  MAX_IMAGE_SIZE    maximum pixels in output image" << endl;
    cout << "   -c  CONFIG_FILE_PATH  location of the config file" << endl;
    cout << endl;
    cout << "Usage Examples: " << endl;
    cout << "   ./handle_object /var/www/hdf5-example-data" << endl;
    cout << "   ./handle_object /var/www/hdf5-example-data/amazing-data/";
    cout << "example-data.h5" << endl;
    cout << "   ./handle_object /var/www/hdf5-example-data/amazing-data/";
    cout << "example-data.h5/scan_1/data_1/image[10:15,20:25]" << endl;
    cout << endl;

    return;
}


// Enable running this application from a terminal
int main(int argc, char **argv)
{

    bool debug = false;
    bool getTreePath = false;
    bool jsonOutput = false;
    int maxImageSize = 1.6e5;
    string configFilePath = "../config/config.cfg";
    vector<string> objectNames;

    // Loop over the arguments
    for (int i = 0; i < argc; i++)
    {

        string arg_i = string(argv[i]);

        // Print the help message
        if (argc == 1 || arg_i == "-h" || arg_i == "-help" ||
            arg_i == "--help")
        {
            printHelp();
            return 0;
        }

        // Look for options and input
        if (i > 0) {

            if (arg_i == "-d") {
                debug = true;
            }

            if (arg_i == "-t") {
                getTreePath = true;
            }

            if (arg_i == "-j") {
                jsonOutput = true;
            }

            if (arg_i == "-m" && argc >= (i+2)) {
                maxImageSize = atof(argv[i+1]);
            }

            if (arg_i == "-c" && argc >= (i+2)) {
                configFilePath = argv[i+1];
            }

            string arg_p = string(argv[i - 1]);;

            if (arg_i == "-m" || arg_p == "-m" || arg_i == "-c" ||
                arg_p == "-c" || arg_i == "-d" || arg_i == "-t" ||
                arg_i == "-j") {
                continue;
            }

            // Save input file or object names
            objectNames.push_back(arg_i);
        }

    }

    if (debug)
    {
        cout << endl;
        cout << "** Input to handle_object **" << endl;
        cout << "  objects:         " << objectNames.size() << endl;
        for (int i = 0; i < objectNames.size(); i++)
        {
            cout << "   objectNames[" << i << "]: " << objectNames[i] << endl;
        }
        cout << "  getTreePath:     " << getTreePath << endl;
        cout << "  jsonOutput:      " << jsonOutput << endl;
        cout << "  maxImageSize:    " << maxImageSize << endl;
        cout << "  configFilePath:  " << configFilePath << endl;
        cout << endl;
    }

    // Print help message if no input given
    if (objectNames.size() == 0) {
        printHelp();
        return 0;
    }

    // Set the HDF5_PLUGIN_PATH environmental variable necessary for using
    // filters
    setHdf5PluginPath(configFilePath, debug);

    if (!getTreePath) {

        // Get the object
        getObject(objectNames[0], maxImageSize, configFilePath, jsonOutput,
            debug);

    } else {

        // Get the tree path
        getObjectTreePath(objectNames[0], configFilePath, jsonOutput, debug);
    }

    return 0;
}
