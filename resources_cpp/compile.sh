#!/bin/bash
#
# This is a helper script for compiling with cmake, as I can never remember the
# cmake options and am much too lazy change directories and delete stuff
#
# The options used in this script seem to be compatible with the following
# cmake versions:
#   3.7.2
#   3.13.4

# Remove old stuff, make new directories
rm -rf bin/ build/
mkdir bin/ build/

# Build the executable, clean up
cd build/
cmake -DCMAKE_BUILD_TYPE=Release -DCONAN=DISABLE ../
cmake --build .
rm -rf *
