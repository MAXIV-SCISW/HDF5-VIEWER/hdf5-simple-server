#!/usr/bin/python

'''
I did not end up using this, but it could come in handy in the future
'''

#####################
# IMPORT LIBRARIES ##
#####################

# CAS stuff
import urllib


def verify_ticket(ticket_string, CAS_SERVER):
    """
    Verifies CAS 3.0+ XML-based authentication ticket and returns extended
    attributes.  Returns username on success and None on failure.
    """

    print 'ticket_string:', ticket_string

    url = '%s/p3/serviceValidate?%s' % (CAS_SERVER, ticket_string)
    page = urllib.urlopen(url)

    print 'url sent to CAS server: ', url

    try:
        from xml.etree import ElementTree
    except ImportError:
        from elementtree import ElementTree

    try:
        user = None
        attributes = {}

        response = page.read()
        # print 'response:', response

        tree = ElementTree.fromstring(response)

        if tree[0].tag.endswith('authenticationSuccess'):

            # print 'success!'

            for element in tree[0]:
                if element.tag.endswith('user'):
                    user = element.text
                elif element.tag.endswith('attributes'):
                    for attribute in element:
                        attributes[attribute.tag.split("}").pop()] = \
                            attribute.text
        else:
            print 'not a successful verification'

        print 'user:', user
        print 'attributes:', attributes
        firstname = str(attributes['firstName'])
        print 'firstname:', firstname

        return {'message': True, 'firstName': firstname}

    finally:
        page.close()
