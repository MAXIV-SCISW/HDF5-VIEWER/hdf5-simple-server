// LIBRARIES
#include <h5cpp/hdf5.hpp>
#include <boost/filesystem.hpp>
#include <iostream>
#include <string>
#include <vector>
#include <math.h>

// NAMESPACES
using std::cout;
using std::endl;
using std::string;
using std::vector;
using namespace hdf5;
using namespace boost;

// STRUCTS
struct getScalarOutput {
    string string_value;
    string object_type;
    string error; 
};

// FUNCTIONS
getScalarOutput getScalar(hdf5::node::Dataset Dataset, bool debug);
