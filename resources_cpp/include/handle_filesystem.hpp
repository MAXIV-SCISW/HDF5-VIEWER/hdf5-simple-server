// LIBRARIES
#include <h5cpp/hdf5.hpp>
#include <boost/filesystem.hpp>
#include <boost/algorithm/string/replace.hpp>
#include <iostream>
#include <string>
#include <vector>
#include <fstream>
#include <string>
#include <algorithm>
#include <sys/stat.h>
#include <unistd.h>
#include <libgen.h>
#include <map>
#include <dirent.h>
#include <math.h>
#include <cstdlib>
#include <stdlib.h>

// NAMESPACES
using std::cout;
using std::endl;
using std::string;
using std::vector;
using std::map;
using namespace hdf5;
using namespace boost;

// STRUCTS
struct getFolderOutput {
    map<int, map<string, string > > file_contents_object;
    map<string, string> general_file_info;
};

struct ParsedPathInfo {
    string object_type;
    string file_path;
    string h5_path;
    vector<int> slice_par;
};

struct HDF5FileInfo {
    bool is_hdf5_file;
    string file_path;
    string h5_path;
};

struct getItemTypeOutput {
    string item_type;
    string file_path; 
    string h5_path;
    string error;
};


// FUNCTIONS
getFolderOutput getHdf5FolderContents(hdf5::node::Group root_group, 
    string file_path, string h5_path, string configFilePath, bool debug);
getFolderOutput getFolderContents(string file_path, string configFilePath, 
    bool debug);
string getDatasetType(hdf5::node::Group root_group, string h5_path, 
    bool debug);
getItemTypeOutput getItemType(string full_path, string configFilePath, 
    bool debug);
string getItemUrl(string full_name, string configFilePath, bool debug);
file::File openFile(string arg_file_path, bool debug);
ParsedPathInfo parseObjectPath(const string& inputString, 
    string configFilePath, bool debug);
vector <string> parseObjectTreePath(const string& inputString, 
    string configFilePath, bool debug);
HDF5FileInfo isHDF5File (const string& name, string configFilePath, 
    bool debug); 
inline bool isHiddenFileOrFolder (const string& name, bool debug); 
inline bool isFile (const string& name); 
inline bool isFolder (const string& name); 
bool canUserReadFile (const string& name);
bool doesFileExist (const string& name); 
bool hasEnding(std::string const &fullString, std::string const &ending); 
vector <int> findSliceParameters(const string& h5_path, bool debug); 
void setHdf5PluginPath(string configFilePath, bool debug);
string cleanFilePathString(string full_path_in);
string shortNameFromFilePath(string full_path_in);
