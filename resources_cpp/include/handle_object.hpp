// LIBRARIES
#include <h5cpp/hdf5.hpp>
#include <boost/filesystem.hpp>
#include <iostream>
#include <string>
#include <vector>
#include <map>

// SOURCE FILES
#include "handle_image.hpp"
// #include "handle_scalar.hpp"
#include "handle_filesystem.hpp"

// NAMESPACES
using std::cout;
using std::endl;
using std::string;
using std::vector;
using std::map;
using std::to_string;
using namespace hdf5;
using namespace boost;

// STRUCTS
struct getDatasetOutput {
    map<string, string> general_file_info;
    vector < vector <float> > image;
    vector <int> shape;
    vector <float> bpr_limits;
    vector <int> section;
};

struct getBasicObjectInfoOutput {
    map<string, string> general_object_info;
    string object_type;
    string item_type;
    string file_path;
    string h5_path;
    vector <int> slice_par;
    bool does_file_exist;
    bool can_user_read_file;
};

// Functions
void getObject(const string& inputString, int maxImageSize, 
    bool jsonOutput, bool debug);
getDatasetOutput getDataset(hdf5::node::Group root_group, string h5_path, 
    vector <int> slice_par, int maxImageSize, bool debug);
getBasicObjectInfoOutput getBasicObjectInfo(const string& inputString,
    string configFilePath, bool debug);
void printJsonReadyOutput(vector< vector <float> > image,
    vector <int> shape, vector <int> section, vector <float> bpr_limits,
    map<string, string> general_file_info,
    map<int, map<string, string > > file_contents_object, bool debug);
void printTerminalOutput(vector< vector <float> > image,
    vector <int> shape, vector <int> section, vector <float> bpr_limits,
    map<string, string> general_file_info,
    map<int, map<string, string > > file_contents_object, bool debug);
void printTreePathOutput(
    map <string, string> general_object_info,
    map <string, map <string, string> > general_contents,
    map <string, map <int, map <string, string > > > tree_path_contents,
    bool debug);
void printHelp();
int main(int argc, char **argv);
