// LIBRARIES
#include <h5cpp/hdf5.hpp>
#include <boost/filesystem.hpp>
#include <iostream>
#include <string>
#include <vector>
#include <math.h>

#include "handle_scalar.hpp"
// NAMESPACES
using std::cout;
using std::endl;
using std::string;
using std::vector;
using namespace hdf5;
using namespace boost;

// STRUCTS
struct getImageOutput {
    vector < vector <float> > image;
    vector <int> shape;
    string object_type;
    vector <float> bpr_limits;
    vector <int> section;
    bool bad_pixels_exist;
    bool is_downsampled;
    string error;
};

struct decimateImageOutput {
    vector < vector <float> > image_ds;
    vector <float> bpr_limits;
    bool bad_pixels_exist;
    bool is_downsampled;
};

struct verifySliceParamtersOutput {
    bool goodSliceDef;
    vector <long unsigned int> sliceParametersOut;
};

// FUNCTIONS
getImageOutput getImage(hdf5::node::Dataset Dataset, vector<int> slice_par, 
    int maxImageSize, bool debug);
decimateImageOutput decimateImage(vector< vector <float> > data_out,
    int maxImageSize, bool debug); 
verifySliceParamtersOutput verifySliceParamters(vector<int> sliceParametersIn,
    vector <int> shape, bool debug);
