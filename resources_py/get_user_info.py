#!/usr/bin/python

'''
A set of functions for determining whether or not a user has the permissions to
read a file.

The user information is taken from the host file system.
'''

#####################
# IMPORT LIBRARIES ##
#####################

# User information
from pwd import getpwnam

# General systems
import argparse
import sys
import json


###########
# HELPERS #
###########

# Global variables
DEBUG_MODE = False
OBJECT_UUID = False


def get_user_info_sys(username, debug):
    '''
    Given a user name, get the user id number, group id numbers
    '''

    if not isinstance(username, str):
        username = str(username.decode())

    if debug:
        print('')
        print('**** START resources/get_user_info.py get_user_info_sys ****')
        print('')
        print('  username:          ' + str(username))

    userid = usergid = usergids = False

    # Get user id number - quick fix, Should really get this from CAS,
    # but that item is not available at this time...
    try:
        userid = getpwnam(username).pw_uid
    except KeyError:
        print('  ** user', username, 'does not exist **')

    if debug:
        print('  userid:            ' + str(userid))

    if userid is not False:

        if debug:
            print('  executing getpwnam(username).pw_gid ')

        # Get the group id number for the user name
        usergid = getpwnam(username).pw_gid

        # if debug:
        #     print '  executing getgrgid'

        # # The group id numbers for all groups for this user
        # # This information may be unnecessary in the end ... but who knows?
        # usergids = [g.gr_gid for g in getgrall() if username in g.gr_mem]
        # usergids.append(getgrgid(usergid).gr_gid)

    if debug:
        print('  usergid:           ' + str(usergid))
        print('  usergids:          ' + str(usergids))
        print('')
        print('**** END resources/get_user_info.py get_user_info_sys ****')
        print('')

    return userid, usergid, usergids


########
# MAIN #
########

def main(argv):
    '''
    The main function - usage and help, argument parsing
    '''

    # Setup options
    parser = argparse.ArgumentParser(
        description='Get the uid and gid numbers for a given username')
    parser.add_argument('-d', '--debug', action='store_true',
                        help='Debug output')
    parser.add_argument("-u", '--username', required=False,
                        default='jasbru',
                        help='thee username to search for')

    # Print a little extra in addition to the standard help message
    if '-h' in argv or '--help' in argv:
        try:
            args = parser.parse_args(['-h'])
        except SystemExit:
            print('')
            print('Examples of usage:')
            print('')
            sys.exit()
    else:
        args = parser.parse_args(argv)

    if args.debug:
        print(args)

    userid, usergid, usergids = get_user_info_sys(args.username,
                                                  args.debug)
    print('get_user_info_sys results:')
    print('  userid: ', userid)
    print('  usergid:', usergid)
    print('  usergids:', usergids)
    print('')


#######################
# RUN THE APPLICATION #
#######################

if __name__ == '__main__':
    main(sys.argv[1:])
