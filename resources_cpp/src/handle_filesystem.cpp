/*
 * This is a collection of functions that list the contents of folders on disk,
 * the cotnents of hdf5 files and folders, an parse  filepath input.
 */


#include "handle_filesystem.hpp"


// Given an items full path name, determine what type of item it is:
//     - folder
//     - hdf5 file
//     - hdf5 file folder
//     - hdf5 dataset
//         - image
//         - image-series
//         - line
//         - number
//         - string
getItemTypeOutput getItemType(string full_path, string configFilePath,
    bool debug)
{
    if (debug) {
        cout << "*** START getItemType ***" << endl;
        cout << "  full_path:      " << full_path << endl;
        cout << "  configFilePath: " << configFilePath << endl;
    }

    string item_type = "h5_unknown";
    string error = "";

    // Remove any trailing slashes
    if (full_path.find_last_of('/') == (full_path.length() - 1)) {
        full_path.pop_back();
    }

    // Find out some basic things about this item
    bool is_hidden = isHiddenFileOrFolder(full_path, debug);
    bool is_file = isFile(full_path);
    bool is_folder = isFolder(full_path);

    if (debug) {
        cout << "    is_hidden:           " << is_hidden << endl;
        cout << "    is_file:             " << is_file << endl;
        cout << "    is_folder:           " << is_folder << endl;
    }

    // See if the given full item path contains a valid hdf5 file path
    HDF5FileInfo isHDF5FileResult = isHDF5File(full_path, configFilePath,
        debug);
    bool is_hdf5_file = isHDF5FileResult.is_hdf5_file;
    string file_path = isHDF5FileResult.file_path;
    string h5_path = isHDF5FileResult.h5_path;

    if (debug) {
        cout << "    is_hdf5_file:        " << is_hdf5_file << endl;
    }

    // Perhaps it's simply a folder on disk.
    if (is_folder) {
        item_type = "folder";

    // Or maybe an hdf5 file
    } else if (is_hdf5_file && is_file && !is_hidden) {
        item_type = "h5_file";
        h5_path = "";

    // Or, perhaps it is an item inside an hdf5 file
    } else if (!is_file && h5_path != "") {
        item_type = "h5_object";

        // Open the file
        file::File DataFile = openFile(file_path, debug);

        // Get the root group of the file
        auto root_group = DataFile.root();

        try {
            // And the item within the file
            auto n = hdf5::node::get_node(root_group, h5_path);

            // Get the item type.
            if (n.type() == hdf5::node::Type(H5O_TYPE_GROUP)) {
                item_type = "h5_folder";
            }
            if (n.type() == hdf5::node::Type(H5O_TYPE_DATASET)) {
                item_type = getDatasetType(root_group, h5_path, debug);
            }
        } catch (std::runtime_error) {
            item_type = "h5_object_error";

            try {
                // Get the group in which the object is locate
                auto b = hdf5::node::get_real_base(root_group, h5_path);
                string short_name = shortNameFromFilePath(full_path);

                if (debug) {
                    cout << "    --> " << b.link().path() << endl;
                    cout << "      + " << b.link().type() << endl;
                    cout << "      + " << short_name << endl;
                }

                // BAD METHOD - could not figure out another way to get the
                // link to this bad node, so I loop through all of them and
                // look for a match.  Then I can check if the link really
                // exists or not.
                bool link_exists = false;
                for(auto l: b.links) {
                    if (short_name == l.path().name()) {
                        link_exists = true;
                        if (!l.is_resolvable()) {
                            item_type = "h5_does_not_exist";
                            error = "Linked to object does not exist";
                        }
                    }
                }
                if (!link_exists) {
                    item_type = "h5_does_not_exist";
                    error = "Object does not exist";
                }

            } catch (std::runtime_error) {
                if (debug) {
                    cout << "    get_real_base error" << endl;
                }
            }
        }
    }

    if (debug) {
        cout << endl;
        cout << "    item_type: " << item_type << endl;
        cout << "***  END  getItemType ***" << endl;
        cout << endl;
    }

    return {item_type, file_path, h5_path, error};
}


// Get the contents of a folder inside an hdf5 file
getFolderOutput getHdf5FolderContents(hdf5::node::Group root_group,
    string file_path, string h5_path, string configFilePath, bool debug)
{
    if (debug) {
        cout << endl;
        cout << "*** START getHdf5FolderContents ***" << endl;
        cout << "  file_path: " << file_path << endl;
        cout << "  h5_path:   " << h5_path << endl;
    }

    // Output variables
    map<int, map<string, string > > file_contents_object;
    map<string, string> general_file_info;
    general_file_info["object_type"] = "h5_folder";

    // Look in the root of the file
    if (debug) {
        cout << endl;
        cout << "  Root contains "<< root_group.nodes.size();
        cout << " node";
        if (root_group.nodes.size() != 1) {
            cout << "s";
        }
        cout << endl;

        try {
            for(auto n: root_group.nodes) {
                try {
                    cout << "    --> " << n.link().path() << endl;
                    cout << "      + " << n.link().type() << endl;
                    cout << "      + " << n.type() << endl;
                } catch (std::runtime_error) {
                    cout << "    *** problem with a node ***" << endl;
                }
            }
        } catch (std::runtime_error) {
            cout << "    *** problem looping over root nodes ***" << endl;
        }
        cout << endl;
    }

    // Look in the specified folder
    try {
        hdf5::node::Group folder = hdf5::node::get_node(root_group, h5_path);

        if (debug) {
            cout << "  " << h5_path << " contains "<< folder.nodes.size();
            cout << " node";
            if (folder.nodes.size() != 1) {
                cout << "s";
            }
            cout << endl;
        }

        try {
            // Loop over the contents of the folder.  It seems best to look at
            // links first and verify that the linked-to object exists, which
            // could be in a different file, and then get the node that the
            // link points to.
            unsigned idx = 0;
            for(auto l: folder.links) {

                string short_name = "";
                string full_path = "";
                string item_type = "h5_unknown";
                string url = "";
                bool does_exist = false;

                if (debug) {
                    cout << "    link info:" << endl;
                    cout << "      + type:        " << l.type() << endl;
                    cout << "      + exists:      " << l.exists() << endl;
                    cout << "      + resolvable:  " << l.is_resolvable() << endl;
                    cout << "      + target path: " << l.target().file_path() << endl;
                    cout << "      + path name:   " << l.path().name() << endl;
                }

                // The short and full path names of the item
                short_name = l.path().name();
                full_path = cleanFilePathString(file_path + "/" + h5_path +
                    "/" + short_name);

                // The url to this item
                url = getItemUrl(full_path, configFilePath, false);

                // Does the item actually exist?
                does_exist = l.is_resolvable() && l.exists();

                if (does_exist) {
                    auto n = l.operator*();

                    try {
                        if (debug) {
                            cout << "    node info:  " << endl;
                            cout << "      + path:   " << n.link().path() << endl;
                            cout << "      + type:   " << n.type() << endl;
                        }

                        // Get the item type.
                        if (n.type() == hdf5::node::Type(H5O_TYPE_GROUP)) {
                            item_type = "h5_folder";
                        }
                        if (n.type() == hdf5::node::Type(H5O_TYPE_DATASET)) {
                            string h5_path_node = h5_path + "/" + short_name;
                            item_type = getDatasetType(root_group,
                                h5_path_node, debug);
                        }

                    } catch (std::runtime_error) {
                        cout << "    *** problem with a folder node ***" << endl;
                    }

                } else {
                    item_type = "h5_does_not_exist";
                    file_contents_object[idx]["error"] =
                        "Linked to object does not exist";
                }

                if (debug) {
                    cout << endl;
                }

                file_contents_object[idx]["short_name"] = short_name;
                file_contents_object[idx]["full_name"] = full_path;
                file_contents_object[idx]["url"] = url;
                file_contents_object[idx]["item_type"] = item_type;
                file_contents_object[idx]["user_can_read"] = "True";
                file_contents_object[idx]["does_exist"] = (does_exist ?
                    "True": "False");

                idx++;
            }

        } catch (std::runtime_error) {

            if (debug) {
                cout << endl;
                cout << "    *** problem looping over folder nodes ***" << endl;
                cout << endl;
            }

            general_file_info["reading_error"] = "True";
        }

        general_file_info["does_exist"] = "True";

    } catch (std::runtime_error) {
        cout << "  " << h5_path << " is NOT a valid folder name! **" << endl;
        general_file_info["does_exist"] = "False";
    }

    if (debug) {
        cout << "***  END  getHdf5FolderContents ***" << endl;
    }

    return {file_contents_object, general_file_info};
}


// For folders on disk, get a list of contents, ignoring non hdf5 files
getFolderOutput getFolderContents(string file_path, string configFilePath,
    bool debug)
{
    if (debug) {
        cout << endl;
        cout << "*** START getFolderContents ***" << endl;
        cout << "  file_path: " << file_path << endl;
    }

    // Output variables
    map<int, map<string, string > > file_contents_object;
    string item_type;
    string user_can_read;
    string short_name;
    string url;
    map<string, string> general_file_info;

    // Remove any trailing slashes
    if (file_path.find_last_of('/') == (file_path.length() - 1)) {
        file_path.pop_back();
    }

    if (debug) {
        cout << "  Looking in folder: " << file_path << endl;
        cout << endl;
    }

    unsigned idx = 0;

    // Open the directory, loop over contents
    if (auto dir = opendir(file_path.c_str())) {
        while (auto f = readdir(dir)) {

            // Skip everything that starts with a dot
            if (!f->d_name || f->d_name[0] == '.') {
                continue;
            }

            if (debug) {
                cout << "  ******************************************" << endl;
                cout << "  Folder Content: " << f->d_name << endl;
            }

            string short_name = f->d_name;
            string full_path = "";
            string item_url = "";
            string item_type = "";
            bool ignore_item = false;
            bool can_user_read_file = false;
            bool is_hdf5_file = false;
            bool is_hidden = false;
            bool is_file = false;
            bool is_folder = false;

            // Assemble the complete file path, note type of file. There should
            // only be two possibilities: folder or hdf5 file. Other file types
            // will be ignored.
            full_path = cleanFilePathString(file_path + "/" + short_name);

            // Does the user have permission to read this file?
            can_user_read_file = canUserReadFile(full_path);

            if (debug) {
                cout << "    full_path:           " << full_path << endl;
                cout << "    can_user_read_file:  " << can_user_read_file;
                cout << endl;
            }

            if (can_user_read_file) {

                // Inspect this object a bit
                is_hidden = isHiddenFileOrFolder(full_path, debug);
                is_file = isFile(full_path);
                is_folder = isFolder(full_path);
                HDF5FileInfo isHDF5FileResult = isHDF5File(full_path,
                    configFilePath, debug);
                is_hdf5_file = isHDF5FileResult.is_hdf5_file;

                if (is_hdf5_file) {
                    item_type = "h5_file";
                }

                if (is_folder) {
                    item_type = "folder";
                }

                if (debug) {
                    cout << "    is_hidden:           " << is_hidden << endl;
                    cout << "    is_file:             " << is_file << endl;
                    cout << "    is_folder:           " << is_folder << endl;
                    cout << "    is_hdf5_file:        " << is_hdf5_file << endl;
                    cout << "    item_type:           " << item_type << endl;
                }

                // Add all folder and hdf5 files to the output map, ignore files
                // which cannot be read by the user
                if (is_hdf5_file || is_folder) {

                    if (debug) {
                        cout << "    ** I like this guy, let's keep him" << endl;
                    }

                    item_url = getItemUrl(full_path, configFilePath, debug);

                    file_contents_object[idx]["short_name"] = short_name;
                    file_contents_object[idx]["full_name"] = full_path;
                    file_contents_object[idx]["url"] = item_url;
                    file_contents_object[idx]["item_type"] = item_type;
                    file_contents_object[idx]["user_can_read"] = "True";

                    idx++;
                } else {
                    if (debug) {
                        cout << endl;
                        cout << "  The file " << full_path;
                        cout << " is not an " << endl;
                        cout << "  HDF5 file or a folder on disk, skipping";
                        cout << endl;
                    }
                }

            } else {
                if (debug) {
                    cout << endl;
                    cout << "  User does not have permission to view ";
                    cout << file_path << ", skipping" << endl;
                }
            }

            if (debug) {
                cout << endl;
            }
        }
        closedir(dir);
    }


    if (debug) {
        cout << "***  END  getFolderContents ***" << endl;
    }

    return {file_contents_object, general_file_info};
}


// Determine the type of dataset, and return a more human readable format than
// the hdf5 types.
//     - line
//     - image
//     - image-series
//     - text
//     - number
string getDatasetType(hdf5::node::Group root_group, string h5_path, bool debug)
{
    if (debug) {
        cout << endl;
        cout << "    *** START getDatasetType ***" << endl;
        cout << "      h5_path: " << h5_path << endl;
    }

    // Output variable
    string dataset_type = "unknown";

    // Get the dataset
    auto Dataset = root_group.get_dataset(h5_path);

    // Try to figure out what type of dataset this is - there must
    // be a better way to do this?!?
    auto Dataspace = Dataset.dataspace();
    auto DataspaceType = Dataspace.type();

    // Scalars - single value datasets
    if (DataspaceType == hdf5::dataspace::Type(H5S_SCALAR)) {
        auto DataType = Dataset.datatype().get_class();

        if (DataType == hdf5::datatype::Class(H5T_STRING)) {
            dataset_type = "text";
        }
        if (DataType == hdf5::datatype::Class(H5T_FLOAT)) {
            dataset_type = "number";
        }
        if (DataType == hdf5::datatype::Class(H5T_INTEGER)) {
            dataset_type = "number";
        }

    }

    // Simple - multidimensional datasets
    if (DataspaceType == hdf5::dataspace::Type(H5S_SIMPLE)) {
        dataspace::Simple Dataspace(Dataset.dataspace());
        auto Dimensions = Dataspace.current_dimensions();
        if (Dimensions.size() == 1) {
            dataset_type = "line";
        }
        if (Dimensions.size() == 2) {
            dataset_type = "image";
        }
        if (Dimensions.size() == 3) {
            dataset_type = "image-series";
        }
    }

    if (debug) {
        cout << "      DataspaceType:  " << DataspaceType << endl;
        cout << "      dataset_type:   " << dataset_type << endl;
        cout << "    ***  END  getDatasetType ***" << endl;
        cout << endl;
    }

    return dataset_type;
}


map<string, string > getValuesFromConfigFile(string configFilePath, bool debug)
{
    debug = false;

    map < string, string > values;
    string value = "";
    string key = "";

    // Read the config file, find the HDF5_PLUGIN_PATH environment variable
    std::ifstream config_file(configFilePath);

    string line;
    while (std::getline(config_file, line))
    {
        if (debug) {
            cout << "line: " << line << endl;
        }

        // Look for key-value pairs separated by =
        key = line.substr(0, line.find("="));
        value = line.substr(line.find("=") + 1);

        // Remove all single-quote characters
        value.erase(remove(value.begin(), value.end(), '\'' ), value.end());
        key.erase(remove(key.begin(), key.end(), '\'' ), key.end());

        if (debug) {
            cout << "key:       " << key << endl;
            cout << "value:     " << value << endl;
        }

        values[key] = value;
    }

    return values;
}


string getItemUrl(string file_path, string configFilePath, bool debug)
{

    if (debug) {
        cout << endl;
        cout << "  *** START getItemUrl ***" << endl;
        cout << "    file_path: " << file_path << endl;
    }

    // Construct the base url from parameters in the config file
    map<string, string > values = getValuesFromConfigFile(configFilePath,
        debug);

    string file_url = values["PROTOCOL"] + "://" + values["HOST_NAME"]
        + ":" + values["PORT"];

    string modified_path = file_path;

    /*
    size_t pos = modified_path.find(values["DATA_DIR"]);
	if (pos != std::string::npos)
	{
		// If found then erase it from string
		modified_path.erase(pos, values["DATA_DIR"].length());
	}
	*/

    file_url += modified_path;

    if (debug) {
        cout << "    protocol  : " << values["PROTOCOL"] << endl;
        cout << "    host_name : " << values["HOST_NAME"] << endl;
        cout << "    port      : " << values["PORT"] << endl;
        cout << "    file_url  : " << file_url << endl;
        cout << "  ***  END  getItemUrl ***" << endl;
        cout << endl;
    }

    return file_url;
}



// Check if the given path name is a file, and if so open it, then return the
// file::File object
file::File openFile(string arg_file_path, bool debug)
{

    if (debug) {
        cout << endl;
        cout << "*** START openFile ***" << endl;
        cout << "  arg_file_path:        " << arg_file_path << endl;
    }

    filesystem::path file_path = arg_file_path;

    // See if this is an HDF5 file
    if(!hdf5::file::is_hdf5_file(file_path)) {
        if (debug) {
            cout << "  Not an HDF5 file :(" << endl;
        }
    } else {
        if (debug) {
            cout << "  It's an HDF5 file!!" << endl;
        }
    }

    // Open the file
    file::File DataFile = file::open(file_path);

    if (debug) {
        cout << endl;
        cout << "***  END openFile ***" << endl;
        cout << endl;
    }

    return DataFile;
}


// Check if this process read rights for the file path given
bool canUserReadFile (const string& name) {
    return (access(name.c_str(), R_OK) == 0);
}


// Check is this file path exists in the file system
bool doesFileExist (const string& name) {
    return (access(name.c_str(), F_OK) == 0);
}


ParsedPathInfo parseObjectPath(const string& inputString, string configFilePath,
    bool debug)
{
    if (debug) {
        cout << endl;
        cout << "*** START parseObjectPath" << endl;
        cout << "  inputString:         " << inputString << endl;
    }

    string object_type = "";
    string file_path = "";
    string h5_path = "";
    vector<int> slice_par;
    bool is_hdf5_file = false;

    bool ignore_file = true;

    // Sorta parse the input string - look for hdf5 files and file objects
    HDF5FileInfo isHDF5FileResult = isHDF5File(inputString, configFilePath,
        debug);

    is_hdf5_file = isHDF5FileResult.is_hdf5_file;
    file_path = isHDF5FileResult.file_path;
    h5_path = isHDF5FileResult.h5_path;

    bool is_hidden = isHiddenFileOrFolder(file_path, debug);
    bool is_folder = isFolder(file_path);
    if (debug) {
        cout << "    is_hidden:           " << is_hidden << endl;
        cout << "    is_folder:           " << is_folder << endl;
    }

    // Determine what type of object we've got here
    if (is_hdf5_file) {
        object_type = "h5_object";
    }

    if (is_folder) {
        object_type = "folder";
    }

    if (is_hidden) {
        object_type = "ignore_file";
    }

    // If this is an hdf5 object, see if slice parameters were supplied
    if (object_type == "h5_object") {

        slice_par = findSliceParameters(h5_path, debug);

        // Remove the slice notation from the object name
        h5_path = h5_path.substr(0, h5_path.find("["));

        if (debug) {
            for (unsigned i=0; i < slice_par.size(); i++) {
                cout << "slice_par[" << i << "]: " << slice_par[i] << endl;
            }
        }
    }

    if (debug) {
        cout << "***  END  parseObjectPath" << endl;
        cout << endl;
    }
    return {object_type, file_path, h5_path, slice_par};
}


string getRelativePathFromFullPath(string full_path, string data_dir,
    bool debug)
{
    string relative_path = "";

    // Remove any slice notation that might be in the object name
    relative_path = full_path.substr(0, full_path.find("["));

    // Remove any trailing slashes
    if (data_dir.find_last_of('/') == (data_dir.length() - 1)) {
        data_dir.pop_back();
    }

    // Remove the data directory path from the full path name
    size_t pos = relative_path.find(data_dir);
    if (pos != std::string::npos)
    {
        relative_path.erase(pos, data_dir.length());
    }

    if (debug) {
        cout << "  relative_path: {" << relative_path << "}" << endl;
    }

    return relative_path;
}


vector <string> parseObjectTreePath(const string& inputString,
    string configFilePath, bool debug)
{
    if (debug) {
        cout << endl;
        cout << "  *** START parseObjectTreePath" << endl;
        cout << endl;
        cout << "  inputString:   {" << inputString << "}" << endl;
    }

    string relative_path = "";
    string h5_path = "";
    vector<string> tree_path;
    vector<int> slice_par;
    bool is_hdf5_file = false;
    bool ignore_file = true;

    // Get the base directory from the configuration file
    map<string, string > configValues = getValuesFromConfigFile(
        configFilePath, debug);
    string data_dir = configValues["DATA_DIR"];

    // Get the relative path from the given path, clean it up a bit too
    relative_path = getRelativePathFromFullPath(inputString, data_dir, debug);

    // The first element of the tree path should be the base directory
    tree_path.push_back(data_dir);

    // Create list of complete path names for each ancestor of this object
    if (relative_path != "") {

        string s = relative_path;

        // Remove any trailing slashes
        if (s.find_last_of('/') == (s.length() - 1)) {
            s.pop_back();
        }

        // Setup splitting of the path name
        string delimiter = "/";
        bool found_match = false;
        if (debug) {
            cout << "  delimiter:  " << delimiter << endl;
            cout << endl;
        }

        size_t pos = 0;
        std::string token;

        // Look for '/' characters, save the full path name for each folder
        while ((pos = s.find(delimiter)) != std::string::npos) {

            token = s.substr(0, pos);

            if (debug) {
                cout << "    ** token: {" << token << "}" << endl;
            }

            // Add to the base directory
            if (token != "") {
                tree_path.push_back(cleanFilePathString(
                    tree_path[tree_path.size() - 1] + delimiter + token));
            }

            s.erase(0, pos + delimiter.length());
            found_match = true;
        }

        // The last bit here should be the object itself, no need to save its
        // path
        if (debug) {
            cout << "    ** s:     {" << s << "}" << endl;
        }
    }

    if (debug) {
        cout << endl;
        for (unsigned i=0; i < tree_path.size(); i++) {
            cout << "      tree_path[" << i << "]: " << tree_path[i] << endl;
        }
        cout << endl;
        cout << "  ***  END  parseObjectTreePath" << endl;
        cout << endl;
    }

    return tree_path;
}


// Simple check for the existance of commonly used hdf5 file extensions.
// If it is an hdf5 file, see if a path within the file is given.
HDF5FileInfo isHDF5File (const string& name, string configFilePath, bool debug) {

    if (debug) {
        cout << endl;
        cout << "  *** START isHDF5File ***" << endl;
    }

    // Output variables
    bool is_hdf5_file = false;
    string file_path = "";
    string h5_path = "";
    vector<string> tree_path;

    // Common hdf5 file extensions
    vector<string> hdf5_extensions = {".h5", ".hdf5", ".hdf", ".nxs", ".nx5",
        ".cxi", ".he5"};

    // Loop over the different extensions
    for (const string &hdf5_extension : hdf5_extensions) {

        if (debug) {
            cout << "  checking for extension: " << hdf5_extension << endl;
        }

        string s = name;
        bool found_match = false;
        bool ends_with_ext = false;

        // If this is a file name, then the file name should be the end of the
        // string, or be followed by a slash (or maybe using a colon is better?)
        ends_with_ext = hasEnding(s, hdf5_extension);
        if (debug) {
            cout << "  ends_with_ext:          " << ends_with_ext << endl;
        }

        if (ends_with_ext) {
            file_path = s;
            is_hdf5_file = true;
        } else {
            size_t pos = 0;
            string token;
            string delimeter = hdf5_extension + "/";

            // See if the input string contains one of these extensions
            while ((pos = s.find(delimeter)) != std::string::npos) {
                token = s.substr(0, pos);
                if (debug) {
                    cout << "    ** token: " <<  token << endl;
                }
                s.erase(0, pos + delimeter.length());
                found_match = true;
                if (debug) {
                    cout << "    found extension match: " << hdf5_extension << endl;
                }
            }

            // If an hdf5 file extension was found, save the file path and any path
            // that might be within the hdf5 file
            if (found_match) {
                if (debug) {
                    cout << "    ** s:     " << s << endl;
                }

                h5_path = s;
                file_path = token + hdf5_extension;
                is_hdf5_file = true;
            }
        }
    }

    if (!is_hdf5_file) {
        file_path = name;
    }

    bool can_user_read_file = canUserReadFile(file_path);

    // Even though the file may have the correct extension, double check that
    // it is actually an hdf5 file
    if (is_hdf5_file && can_user_read_file) {
        if(!hdf5::file::is_hdf5_file(file_path)) {
            is_hdf5_file = false;

            if (debug) {
                cout << "  This file has a correct extension, but does not ";
                cout << "seem to be a valid HDF5 file." << endl;
            }
        }
    }

    if (debug) {
        cout << endl;
        cout << "  Done checking if this is an hdf5 file, results:" << endl;
        cout << "    is_hdf5_file:        " << is_hdf5_file << endl;
        cout << "    file_path:           " << file_path << endl;
        cout << "    h5_path:             " << h5_path << endl;
        cout << "    can_user_read_file:  " << can_user_read_file << endl;
        cout << "  ***  END  isHDF5File ***" << endl;
        cout << endl;
    }

    return {is_hdf5_file, file_path, h5_path};
}


// Check if this path name (file or folder) is hidden or not
inline bool isHiddenFileOrFolder (const string& name, bool debug) {

    if (debug) {
        cout << "  *** START isHiddenFileOrFolder ***" << endl;
    }

    bool is_hidden = false;

    char* file_name1 = strdup(name.c_str());
    char* file_name2 = strdup(name.c_str());

    char* foldername = dirname(file_name1);
    char* filename = basename(file_name2);

    if (debug) {
        cout << "    foldername:          " << foldername << endl;
        cout << "    filename:            " << filename << endl;
    }

    // Check if the file name starts with a period
    if (string(filename).find(".") == 0)
    {
        is_hidden = true;
    }

    if (debug) {
        cout << "  ***  END  isHiddenFileOrFolder ***" << endl;
    }
    return is_hidden;
}


// Check of the object in the given path name is a file
inline bool isFile (const string& name) {

    bool returnValue = false;

    struct stat s;

    if(stat(name.c_str(), &s) == 0)
    {
        if(s.st_mode & S_IFREG)
        {
            returnValue = true;
        }
    }

    return returnValue;
}


// Check of the object in the given path name is a folder
inline bool isFolder (const string& name) {

    bool returnValue = false;

    struct stat s;

    if(stat(name.c_str(), &s) == 0)
    {
        if(s.st_mode & S_IFDIR)
        {
            returnValue = true;
        }
    }

    return returnValue;
}


// Look for slice paramaters in the hdf5 object path
vector<int> findSliceParameters(const string& h5_path, bool debug) {

    vector<int> slice_parameters;
    vector<string> slice_pairs;
    string strNew = "";

    string str = h5_path;

    // Find characters denoting slices
    unsigned pos_first = str.find("[");
    unsigned pos_last = str.find("]");
    unsigned length = pos_last - pos_first - 1;

    if (pos_first != pos_last) {
        strNew = str.substr(pos_first + 1, length);
    }

    // If the brackets denoting slice parameters have been found above,
    // extract the parameters
    if (strNew != "") {
        size_t pos = 0;
        string token;
        string delimiter = ",";
        while ((pos = strNew.find(delimiter)) != std::string::npos) {
            token = strNew.substr(0, pos);
            strNew.erase(0, pos + delimiter.length());
            slice_pairs.push_back(token);
        }
        slice_pairs.push_back(strNew);

        // Loop over the pairs of parameters, split on : delimeter
        for (string slice_pair : slice_pairs) {

            pos = 0;
            token;
            delimiter = ":";
            string s = slice_pair;
            while ((pos = slice_pair.find(delimiter)) != std::string::npos) {
                token = slice_pair.substr(0, pos);
                slice_pair.erase(0, pos + delimiter.length());
                slice_parameters.push_back(std::stoi(token));
            }
            slice_parameters.push_back(std::stoi(slice_pair));

        }
    }

    return slice_parameters;
}


string getEnvVar(std::string const & key)
{
    char * val = getenv( key.c_str() );
    return val == NULL ? string("") : string(val);
}


void setEnvVar(string key, string value, bool debug)
{
    if (debug) {
        cout << "** Setting environmental variable **" << endl;
        cout << "  key:   " << key << endl;
        cout << "  value: " << value << endl;
    }

    setenv(key.c_str(), value.c_str(), true );

    if (debug) {
        cout << "  environmental variable set" << endl;
        cout << "    " << key << ": " << getEnvVar(key) << endl;
        cout << endl;
    }
}


void setHdf5PluginPath(string configFilePath, bool debug)
{

    // Get the value of the environment variable from the config file
    map<string, string > values = getValuesFromConfigFile(configFilePath,
        debug);

    // Set the environment variable
    setEnvVar("HDF5_PLUGIN_PATH", values["HDF5_PLUGIN_PATH"], debug);
}


// A function to remove multiple slashes that might creep in to path names...
string cleanFilePathString(string full_path_in)
{
    bool debug = false;

    if (debug) {
        cout << "*** START handle_filesystem.cpp cleanFilePathString **"<< endl;
        cout << "  full_path_in: " << full_path_in << endl;
    }

    string full_path_out = full_path_in;

    // Look for pairs of slahes, replace with single instance un til there are
    // no more pairs
    while (full_path_out.find("//") != std::string::npos) {
        boost::replace_all(full_path_out, "//", "/");
    }

    if (debug) {
        cout << "  full_path_out: " << full_path_out << endl;
    }

    // Remove trailing slashes
    if (full_path_out.find_last_of('/') == full_path_out.length()-1) {
        full_path_out.pop_back();
    }

    if (debug) {
        cout << "  full_path_out: " << full_path_out << endl;
    }

    return full_path_out;
}


string parentNameFromFilePath(string full_path_in)
{
    if (!full_path_in.empty()) {
        if (full_path_in.find_last_of('/') == full_path_in.length()-1) {
            full_path_in.pop_back();
        }
    }
    string short_name = boost::filesystem::path(
        full_path_in).filename().string();

    return short_name;
}


string shortNameFromFilePath(string full_path_in)
{
    if (!full_path_in.empty()) {
        if (full_path_in.find_last_of('/') == full_path_in.length()-1) {
            full_path_in.pop_back();
        }
    }
    string short_name = boost::filesystem::path(
        full_path_in).filename().string();

    return short_name;
}


bool hasEnding(std::string const &fullString, std::string const &ending) {
    if (fullString.length() >= ending.length()) {
        return (0 == fullString.compare(fullString.length() - ending.length(),
            ending.length(), ending));
    } else {
        return false;
    }
}

